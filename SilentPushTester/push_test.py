import json
import urllib2
import sys
import random
import argparse

parser = argparse.ArgumentParser(description='Generates iOS silent push notifications.')
parser.add_argument('-l', '--loud', help="Generate loud push instead of silent", action="store_true")
parser.add_argument('-d', '--device', help="Send the push only to the specific device")
parser.add_argument('-t', '--devenv', help="Send the push to dev environment (default: enterprise)", action="store_true")
parser.add_argument('-b', '--badge', help="Increment the badge", action="store_true")
parser.add_argument('-s', '--sound', help="Add empty sound", action="store_true")
parser.add_argument('phone', help="Phone number of the callee in e164 format")
args = parser.parse_args()

if (args.devenv):
    app_key = "xNdpVjmccPG7ePFMWaAXNp6Qg3dP9LDmZQa07XhD"
    rest_key = "1CqYhecPvJNI6N8DnnECWguSGTUb0dsTLoFDUE32"
else:
    app_key = "F2qyppnnUvcVqhh4VXGz8mnuwSOTC1oResGrW1MQ"
    rest_key = "L4pPj0fkivERroNX1tqmDlCpI1pWmJWG5cPt7LH2"

alertText = args.phone + " is calling (" + str(random.randint(0,100000)) + ")"
loudAlertText = ''

if (args.loud):
    loudAlertText = alertText

data = {
        "where": {
          "deviceType": "ios"
        },
         "data": {
          "alert": loudAlertText,
          "silent_alert": alertText,
          "content-available" : 1
        }
}

if (args.badge):
    data['data']['badge'] = 'Increment';

if (args.sound):
    data['data']['sound'] = '';

if (args.device != None):
    data['where']['channels'] = { '$in': [args.device] }

#print json.dumps(data)

print "Sending message :" + alertText
req = urllib2.Request("https://api.parse.com/1/push",
                      headers = {
        "X-Parse-Application-Id": app_key,
        "X-Parse-REST-API-Key": rest_key,
        "Content-Type": "application/json",

        # Some extra headers for fun
        "Accept": "*/*",   # curl does this
        "User-Agent": "my-python-app/1", # otherwise it uses "Python-urllib/..."
        },
                      data = json.dumps(data))

f = urllib2.urlopen(req)

print "Server response: " + str(f.getcode())