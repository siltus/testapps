//
//  main.m
//  SilentPushTester
//
//  Created by Sagi Iltus on 4/30/14.
//  Copyright (c) 2014 Vonage. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VGAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([VGAppDelegate class]));
    }
}
