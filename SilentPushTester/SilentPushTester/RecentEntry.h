//
//  RecentEntry.h
//  SilentPushTester
//
//  Created by Sagi Iltus on 5/1/14.
//  Copyright (c) 2014 Vonage. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface RecentEntry : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * content;

@end
