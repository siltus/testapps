//
//  Utils.m
//  SilentPushTester
//
//  Created by Sagi Iltus on 5/1/14.
//  Copyright (c) 2014 Vonage. All rights reserved.
//

#import "Utils.h"
#import <sys/utsname.h> // Used to detect device type (i.e 3GS...)
#import "mach/mach.h"

@implementation Utils

+ (NSString*)shortID
{
    return [@"T" stringByAppendingString:[[[[UIDevice currentDevice] identifierForVendor] UUIDString] substringToIndex:5]];
}

+ (NSString*)deviceTypeFullName
{
    NSDictionary* deviceMapping = @{@"iPhone1,1" : @"iPhone 2G",
                                    @"iPhone1,2" : @"iPhone 3G",
                                    @"iPhone2,1" : @"iPhone 3GS",
                                    @"iPhone3,1" : @"iPhone 4",
                                    @"iPhone3,2" : @"iPhone 4",
                                    @"iPhone3,3" : @"iPhone 4 (CDMA)",
                                    @"iPhone4,1" : @"iPhone 4S",
                                    @"iPhone5,1" : @"iPhone 5",
                                    @"iPhone5,2" : @"iPhone 5 (GSM+CDMA)",
                                    @"iPhone5,3" : @"iPhone 5c",
                                    @"iPhone5,4" : @"iPhone 5c",
                                    @"iPhone6,1" : @"iPhone 5s",
                                    @"iPhone6,2" : @"iPhone 5s",
                                    
                                    @"iPod1,1" : @"iPod Touch (1 Gen)",
                                    @"iPod2,1" : @"iPod Touch (2 Gen)",
                                    @"iPod3,1" : @"iPod Touch (3 Gen)",
                                    @"iPod4,1" : @"iPod Touch (4 Gen)",
                                    @"iPod5,1" : @"iPod Touch (5 Gen)",
                                    
                                    @"iPad1,1" : @"iPad",
                                    @"iPad1,2" : @"iPad 3G",
                                    @"iPad2,1" : @"iPad 2 (WiFi)",
                                    @"iPad2,2" : @"iPad 2",
                                    @"iPad2,3" : @"iPad 2 (CDMA)",
                                    @"iPad2,4" : @"iPad 2",
                                    @"iPad2,5" : @"iPad Mini (WiFi)",
                                    @"iPad2,6" : @"iPad Mini",
                                    @"iPad2,7" : @"iPad Mini (GSM+CDMA)",
                                    @"iPad3,1" : @"iPad 3 (WiFi)",
                                    @"iPad3,2" : @"iPad 3 (GSM+CDMA)",
                                    @"iPad3,3" : @"iPad 3",
                                    @"iPad3,4" : @"iPad 4 (WiFi)",
                                    @"iPad3,5" : @"iPad 4",
                                    @"iPad3,6" : @"iPad 4 (GSM+CDMA)"};
    
    NSString* machineName = [Utils machineName];
    NSString* deviceName = [deviceMapping objectForKey:machineName];
    
    if (deviceName == nil)
        return machineName;
    else
        return deviceName;
}

+ (NSString*)machineName
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    /*
     
     refernce of possible values
     
     @"iPod1,1"   on iPod Touch
     @"iPod2,1"   on iPod Touch Second Generation
     @"iPod3,1"   on iPod Touch Third Generation
     @"iPod4,1"   on iPod Touch Fourth Generation
     @"iPhone1,1" on iPhone
     @"iPhone1,2" on iPhone 3G
     @"iPhone2,1" on iPhone 3GS
     @"iPad1,1"   on iPad
     @"iPad2,1"   on iPad 2
     @"iPhone3,1" on iPhone 4
     @"iPhone4,1" on iPhone 4S
     
     */
    
    return [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
}

vm_size_t usedMemoryNative(void) {
    struct task_basic_info info;
    mach_msg_type_number_t size = sizeof(info);
    kern_return_t kerr = task_info(mach_task_self(), TASK_BASIC_INFO, (task_info_t)&info, &size);
    return (kerr == KERN_SUCCESS) ? info.virtual_size : 0; // size in bytes
}

vm_size_t freeMemoryNative(void) {
    mach_port_t host_port = mach_host_self();
    mach_msg_type_number_t host_size = sizeof(vm_statistics_data_t) / sizeof(integer_t);
    vm_size_t pagesize;
    vm_statistics_data_t vm_stat;
    
    host_page_size(host_port, &pagesize);
    (void) host_statistics(host_port, HOST_VM_INFO, (host_info_t)&vm_stat, &host_size);
    return vm_stat.free_count * pagesize;
}

+ (unsigned int) usedMemory
{
    return usedMemoryNative();
}

+ (unsigned int) freeMemory
{
    return freeMemoryNative();
}

+ (NSString *)trimSpecialCharactersAndSpaces:(NSString *)searchString
{
    // just validate that the string in not nil to prevent a crash
    if (searchString != nil && !([searchString isEqualToString:@","])) {
        NSError *error;
        // Trimming All Characters After the "," character:
        NSRange range = NSMakeRange(0, [searchString length]);
        range = [searchString rangeOfString:@"," options:0 range:range];
        if(range.location != NSNotFound) {
            searchString = [searchString substringToIndex:range.location];
        }
        //trim all the special characters
        NSRegularExpression *regex = [[NSRegularExpression alloc] initWithPattern:@"[^a-zA-Z0-9]" options:NSRegularExpressionCaseInsensitive error:&error];
        
        return [regex stringByReplacingMatchesInString:searchString options:0 range:NSMakeRange(0, [searchString length]) withTemplate:@""];
    }
    
    return nil;
}

@end
