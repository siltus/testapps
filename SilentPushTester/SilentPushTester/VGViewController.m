//
//  VGViewController.m
//  SilentPushTester
//
//  Created by Sagi Iltus on 4/30/14.
//  Copyright (c) 2014 Vonage. All rights reserved.
//

#define DATA_CHUNK_IN_KB 4
#define DATA_CHUNKS DATA_CHUNK_IN_KB*1024

#import "VGViewController.h"
#import "Utils.h"
#import "RecentEntry.h"
#import "VGRecentsManager.h"

@interface VGViewController ()
@property (weak, nonatomic) IBOutlet UILabel *labelMemUsage;
@property (weak, nonatomic) IBOutlet UILabel *labelSessionCount;
@property (weak, nonatomic) IBOutlet UILabel *labelAllTimeCount;
@property (weak, nonatomic) IBOutlet UITableView *tableNotifications;
@property (weak, nonatomic) IBOutlet UILabel *labelDeviceID;

@end

@implementation VGViewController
{
    NSTimer* _updateTimer;
    unsigned int _sessionCount;
    NSMutableArray* _pushList;
    VGRecentsManager* _recentsManager;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"[%s]", __FUNCTION__);
    _sessionCount = 0;
    _pushList = [[NSMutableArray alloc] init];
    _recentsManager = [[VGRecentsManager alloc] init];
    self.tableNotifications.dataSource = self;
    [self updateMemoryUsageLabel];
    self.labelAllTimeCount.text = [NSString stringWithFormat:@"%d", [_recentsManager getNumOfEntries]];
    _updateTimer = [NSTimer scheduledTimerWithTimeInterval:10.0f target:self selector:@selector(updateMemoryUsageLabel) userInfo:nil repeats:YES];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receivedRemoteNotificationIOS6:)
                                                 name:@"remote_notification_ios6" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receivedRemoteNotificationIOS7:)
                                                 name:@"remote_notification_ios7" object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receivedLocalNotification:)
                                                 name:@"local_notification" object:nil];
    
    self.labelDeviceID.text = [Utils shortID];
    
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    NSLog(@"[%s]", __FUNCTION__);
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)receivedRemoteNotificationIOS6:(NSNotification*)notification
{
    @synchronized(self){
        NSLog(@"[%s]", __FUNCTION__);
        _sessionCount++;
        dispatch_async(dispatch_get_main_queue(), ^{
            self.labelSessionCount.text = [NSString stringWithFormat:@"%d", _sessionCount];
            
            NSDictionary* aps = [notification.userInfo objectForKey:@"aps"];
            if (aps)
            {
                NSString* alert = [aps objectForKey:@"alert"];
                
                if (alert)
                {
                    [_pushList addObject:@{@"type": @"Loud Remote", @"content" : alert}];
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:([_pushList count]-1) inSection:0];
                    [self.tableNotifications insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
                    
                    [_recentsManager addNewEntry:alert];
                    self.labelAllTimeCount.text = [NSString stringWithFormat:@"%d", [_recentsManager getNumOfEntries]];
                }
                else
                {
                    NSLog(@"[%s] - No alert message", __FUNCTION__);
                }
            }
            else
            {
                NSLog(@"[%s] - Bad push dictionary", __FUNCTION__);
            }
        });
    }
}

-(void)receivedRemoteNotificationIOS7:(NSNotification*)notification
{
    @synchronized(self){
        NSLog(@"[%s]", __FUNCTION__);
        _sessionCount++;
        
        NSString* message = [notification.userInfo objectForKey:@"silent_alert"];
        
        if ([message length] > 0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.labelSessionCount.text = [NSString stringWithFormat:@"%d", _sessionCount];
                
                NSString* type = @"Silent Remote";
                NSDictionary* aps = [notification.userInfo objectForKey:@"aps"];
                if (aps)
                {
                    if ([[aps objectForKey:@"alert"] length] > 0)
                    {
                        type = @"Loud Remote";
                    }
                }
                
                NSLog(@"[%s] Push type: [%@]", __FUNCTION__, type);
                [_pushList addObject:@{@"type": type, @"content" : message}];
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:([_pushList count]-1) inSection:0];
                [self.tableNotifications insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
                
                [_recentsManager addNewEntry:message];
                self.labelAllTimeCount.text = [NSString stringWithFormat:@"%d", [_recentsManager getNumOfEntries]];
            });
        }
        else
        {
            NSLog(@"[%s] unsupported push for scenario", __FUNCTION__);
        }
    }
}

-(void)receivedLocalNotification:(NSNotification*)notification
{
    @synchronized(self){
        NSLog(@"[%s]", __FUNCTION__);
        _sessionCount++;
        dispatch_async(dispatch_get_main_queue(), ^{
            self.labelSessionCount.text = [NSString stringWithFormat:@"%d", _sessionCount];
            [_pushList addObject:@{@"type": @"Local", @"content" : [notification.userInfo objectForKey:@"content"]}];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:([_pushList count]-1) inSection:0];
            [self.tableNotifications insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
            
            [_recentsManager addNewEntry:[notification.userInfo objectForKey:@"content"]];
            self.labelAllTimeCount.text = [NSString stringWithFormat:@"%d", [_recentsManager getNumOfEntries]];
        });
    }
}

- (IBAction)allocateMemoryBtnPressed:(UIButton *)sender {
    NSLog(@"[%s]", __FUNCTION__);
    [sender setEnabled:NO];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        NSLog(@"Allocator - start allocating" );
        NSMutableArray* arr = [[NSMutableArray alloc] init];
        int i = 0;
        while (i * DATA_CHUNKS < 550 * 1024 * 1024)
        {
            void* temp = malloc(DATA_CHUNKS);
            if (!temp)
            {
                break;
            }
            NSData* data = [[NSData alloc] initWithBytesNoCopy:temp length:DATA_CHUNKS];
            [arr addObject:data];
            i++;
            if (i % 10000 == 0)
            {
                NSLog(@"Allocator - allocated %d KB", i * DATA_CHUNK_IN_KB );
            }
        }
        NSLog(@"Allocator - end - allocated %d KB", i * DATA_CHUNK_IN_KB );
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self updateMemoryUsageLabel];
        });
        
        NSLog(@"Used memory: %d\t\tFreeMemory: %d", [Utils usedMemory], [Utils freeMemory]);
    });

}

- (void)updateMemoryUsageLabel
{
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle]; // this line is important!
    
    NSString *formatted = [formatter stringFromNumber:[NSNumber numberWithInteger:[Utils usedMemory]]];

    self.labelMemUsage.text = formatted;
    
    NSLog(@"Used memory: %d\t\tFreeMemory: %d", [Utils usedMemory], [Utils freeMemory]);
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *cellIdentifier = @"PushCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    

    NSDictionary *text = [_pushList objectAtIndex:indexPath.row];
    [cell.detailTextLabel setText:[NSString stringWithFormat:@"%@: %@", [NSDate date] ,[text objectForKey:@"type"]]];
    [cell.textLabel setText:[text objectForKey:@"content"]];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_pushList count];
}

- (IBAction)resetSessionPressed:(id)sender {
    NSLog(@"[%s]", __FUNCTION__);
    [_pushList removeAllObjects];
    [_tableNotifications reloadData];
    [_labelSessionCount setText:@"0"];
    _sessionCount = 0;
}

- (IBAction)resetAllTimeCount:(id)sender {
    NSLog(@"[%s]", __FUNCTION__);
    [_recentsManager removeAllEntries];
    [_labelAllTimeCount setText:@"0"];
}

@end
