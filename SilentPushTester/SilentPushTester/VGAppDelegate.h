//
//  VGAppDelegate.h
//  SilentPushTester
//
//  Created by Sagi Iltus on 4/30/14.
//  Copyright (c) 2014 Vonage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
