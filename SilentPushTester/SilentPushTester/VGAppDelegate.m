//
//  VGAppDelegate.m
//  SilentPushTester
//
//  Created by Sagi Iltus on 4/30/14.
//  Copyright (c) 2014 Vonage. All rights reserved.
//

#import "VGAppDelegate.h"
#import <Parse/Parse.h>
#import <AddressBook/AddressBook.h>

#import "Utils.h"

@implementation VGAppDelegate
{
    //NSDictionary* _contacts;
    ABAddressBookRef _addressBookRef;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSLog(@"====================================================================================");
    NSLog(@"====================================================================================");
    NSLog(@"====================================================================================");
    NSLog(@"====================================================================================");
    NSLog(@"====================================================================================");
    NSLog(@"====================================================================================");
    NSLog(@"[%s] launchOptions: %@", __FUNCTION__, launchOptions);
    NSLog(@"Client iOS Version: %@", [[UIDevice currentDevice] systemVersion]);
    NSLog(@"Client Device Type: %@", [Utils deviceTypeFullName]);
    NSLog(@"Client Device Name: %@", [[UIDevice currentDevice] name]);
    NSLog(@"Client Device UniqueID: %@", [Utils shortID] );
    
    

    // Override point for customization after application launch.
    
    [application registerForRemoteNotificationTypes: UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeNewsstandContentAvailability];

#ifdef ENTERPRISE_BUILD
    NSLog(@"Push registration - Enterprise build");
    [Parse setApplicationId:@"F2qyppnnUvcVqhh4VXGz8mnuwSOTC1oResGrW1MQ"
                  clientKey:@"F96AxqRsEyDgozX156eMPWHU8rGWHqlbOiVQnoW9"];
#else
    NSLog(@"Push registration - Debug build");
    [Parse setApplicationId:@"xNdpVjmccPG7ePFMWaAXNp6Qg3dP9LDmZQa07XhD"
                  clientKey:@"bagZhHtONoy9nRsz1wgKs3xLspNs9p7c36ORvid6"];
#endif
    
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation addUniqueObject:[Utils shortID] forKey:@"channels"];
    [currentInstallation saveInBackground];
    

    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    _addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
    ABAddressBookRequestAccessWithCompletion(_addressBookRef, ^(bool granted, CFErrorRef error) {
        NSAssert(granted, @"Couldn't get permission to Address Book!");
    });
    
    return YES;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
//    curl -X POST \
//    -H "X-Parse-Application-Id: xNdpVjmccPG7ePFMWaAXNp6Qg3dP9LDmZQa07XhD" \
//    -H "X-Parse-REST-API-Key: 1CqYhecPvJNI6N8DnnECWguSGTUb0dsTLoFDUE32" \
//    -H "Content-Type: application/json" \
//    -d '{
//    "where": {
//        "deviceType": "ios"
//    },
//    "data": {
//        "silent_alert": "+972506322586 is calling you.",
//        "badge" : "Increment",
//        "content-available" : 1
//    }
//}' \
//https://api.parse.com/1/push
    
    NSLog(@"[%s] received push dictionary: %@", __FUNCTION__, userInfo);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"remote_notification_ios6" object:nil userInfo:userInfo];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSLog(@"****************************************************************************************************");
    NSLog(@"****************************************************************************************************");
    NSLog(@"****************************************************************************************************");
    NSLog(@"****************************************************************************************************");
    NSLog(@"****************************************************************************************************");
    NSLog(@"****************************************************************************************************");
    NSLog(@"[%s] received iOS7 push dictionary: %@", __FUNCTION__, userInfo);
    
    [self showContactNotificationFromPush:userInfo];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"remote_notification_ios7" object:nil userInfo:userInfo];
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)showContactNotificationFromPush: (NSDictionary*)userInfo
{
    NSLog(@"[%s]", __FUNCTION__);
    NSString* originalMessage = [userInfo objectForKey:@"silent_alert"];
    
    if ([originalMessage length] == 0)
    {
        NSLog(@"[%s] Original push is not supported", __FUNCTION__);
        return;
    }
    
    NSRange range = [originalMessage rangeOfString:@" "];
    if (range.location < originalMessage.length)
    {
        NSString* e164Num = [originalMessage substringToIndex:range.location];
        NSString* rest = [originalMessage substringFromIndex:range.location];
        
        NSString* contactName = [self getNameForPhoneNumber:e164Num];
        
        if (contactName)
        {
            originalMessage = [NSString stringWithFormat:@"%@%@", contactName, rest];
        }
    }
    
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    
    localNotification.fireDate = [NSDate date];
    localNotification.alertBody = [@"# " stringByAppendingString: originalMessage];
    localNotification.applicationIconBadgeNumber = 1;

    NSLog(@"[%s] scheduling local notification: %@", __FUNCTION__, originalMessage);
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

- (NSString*) getNameForPhoneNumber: (NSString*) number
{
    CFIndex nPeople = ABAddressBookGetPersonCount(_addressBookRef);
    NSAssert(_addressBookRef && (nPeople != -1), @"contactListThread.initializeContactsFromAddressBook - Using ABAddressBookCreate without permission");
    
    CFMutableArrayRef nonMutableContacts = (CFMutableArrayRef) ABAddressBookCopyArrayOfAllPeople(_addressBookRef);
    // This can be done at the time of app launch than when the view is called.
    CFArraySortValues(nonMutableContacts,
                      CFRangeMake(0, CFArrayGetCount(nonMutableContacts)),
                      (CFComparatorFunction)ABPersonComparePeopleByName,
                      (void *)ABPersonGetSortOrdering());
    
    ABRecordRef recordRef = nil;
    NSInteger contactIDToBeChecked = 0;
    
    for (int i=0; i < nPeople; i++)
    {
        recordRef = CFArrayGetValueAtIndex(nonMutableContacts, i);
        contactIDToBeChecked = (NSInteger) ABRecordGetRecordID(recordRef);
        
        if (recordRef)
        {
            NSString *displayName       =   (__bridge_transfer NSString *)ABRecordCopyCompositeName(recordRef);
            
            ABMultiValueRef phoneNumbers = ABRecordCopyValue(recordRef, kABPersonPhoneProperty);
            NSArray* e164Numbers = [self createNewPhoneNumberFromABMultiValueRef:phoneNumbers];
            
            for (NSString* num in e164Numbers)
            {
                if ([number isEqualToString:num])
                {
                    NSLog(@"[%s] - found match for [%@] : [%@]", __FUNCTION__, number, displayName);
                    return displayName;
                }
            }
            if (phoneNumbers != NULL)
                CFRelease(phoneNumbers);
        }
    }
    NSLog(@"[%s] - no match found for [%@]", __FUNCTION__, number);
    return nil;
}

- (NSArray*)createNewPhoneNumberFromABMultiValueRef:(ABMultiValueRef)phoneNumbers
{
    NSMutableArray* arr = [[NSMutableArray alloc] init];
    for (CFIndex i = 0; i <ABMultiValueGetCount(phoneNumbers); i++)
    {
        //Add all the phoneNumbers
        
        CFStringRef phoneNumberValue         = ABMultiValueCopyValueAtIndex(phoneNumbers, i);
        NSString *e164Format                 = (__bridge NSString *)phoneNumberValue;
        
        if([e164Format hasPrefix:@"+"])
        {
            e164Format = [Utils trimSpecialCharactersAndSpaces:e164Format];
            e164Format = [NSString stringWithFormat:@"+%@",e164Format];
        }
        else
        {
            e164Format = [Utils trimSpecialCharactersAndSpaces:e164Format];
        }
        
        if (e164Format)
        {
            [arr addObject:e164Format];
        }
        // Bug 8834 Checking before releasing is important or else the app crashes. Putting checks in everywhere CFRelease is called in this file
        if (phoneNumberValue != NULL)
            CFRelease(phoneNumberValue);
    }
    
    return [NSArray arrayWithArray:arr];
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
     NSLog(@"[%s] received local push: %@", __FUNCTION__, notification.alertBody);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"local_notification" object:nil userInfo:@{@"content" : notification.alertBody}];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    NSLog(@"[%s]", __FUNCTION__);
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    NSLog(@"[%s]", __FUNCTION__);
    NSLog(@"Used memory: %d\t\tFreeMemory: %d", [Utils usedMemory], [Utils freeMemory]);
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    NSLog(@"[%s]", __FUNCTION__);
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    NSLog(@"[%s]", __FUNCTION__);
    NSLog(@"Used memory: %d\t\tFreeMemory: %d", [Utils usedMemory], [Utils freeMemory]);
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    NSLog(@"[%s]", __FUNCTION__);
    NSLog(@"Used memory: %d\t\tFreeMemory: %d", [Utils usedMemory], [Utils freeMemory]);
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
    NSLog(@"[%s]", __FUNCTION__);
    NSLog(@"Used memory: %d\t\tFreeMemory: %d", [Utils usedMemory], [Utils freeMemory]);
}

@end
