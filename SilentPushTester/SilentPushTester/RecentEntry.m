//
//  RecentEntry.m
//  SilentPushTester
//
//  Created by Sagi Iltus on 5/1/14.
//  Copyright (c) 2014 Vonage. All rights reserved.
//

#import "RecentEntry.h"


@implementation RecentEntry

@dynamic date;
@dynamic content;

@end
