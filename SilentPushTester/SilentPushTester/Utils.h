//
//  Utils.h
//  SilentPushTester
//
//  Created by Sagi Iltus on 5/1/14.
//  Copyright (c) 2014 Vonage. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

+ (NSString*)deviceTypeFullName;
+ (unsigned int)usedMemory;
+ (unsigned int)freeMemory;
+ (NSString*)shortID;

+ (NSString *)trimSpecialCharactersAndSpaces:(NSString *)searchString;
@end
