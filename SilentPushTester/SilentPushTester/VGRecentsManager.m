//
//  VGRecentsManager.m
//  SilentPushTester
//
//  Created by Sagi Iltus on 5/1/14.
//  Copyright (c) 2014 Vonage. All rights reserved.
//

#import "VGRecentsManager.h"
#import "RecentEntry.h"

@implementation VGRecentsManager

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;

@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

-(void)addNewEntry:(NSString*)content
{
    NSLog(@"[%s] with content: %@", __FUNCTION__, content);
    RecentEntry* entry = [NSEntityDescription insertNewObjectForEntityForName:@"RecentEntry" inManagedObjectContext:self.managedObjectContext];
    
    entry.content = content;
    entry.date = [NSDate date];
    
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"[%s] ERROR, couldn't save entry!: %@", __FUNCTION__, [error localizedDescription]);
    }
}

-(void)removeAllEntries
{
    NSLog(@"[%s]", __FUNCTION__);
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"RecentEntry" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *items = [_managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error != nil)
    {
        NSLog(@"[%s] Error getting entries - error:%@",__FUNCTION__,error);
    }

    for (NSManagedObject *managedObject in items) {
    	[_managedObjectContext deleteObject:managedObject];
    }
    
    if (![_managedObjectContext save:&error]) {
    	NSLog(@"[%s] Error deleting - error:%@",__FUNCTION__,error);
    }
}

-(unsigned int)getNumOfEntries
{
    NSLog(@"[%s]", __FUNCTION__);
    // initializing NSFetchRequest
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    //Setting Entity to be Queried
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"RecentEntry"
                                              inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSError* error = nil;
    
    // Query on managedObjectContext With Generated fetchRequest
    NSArray *fetchedRecords = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error != nil)
    {
        NSLog(@"[%s] Error getting entries - error:%@",__FUNCTION__,error);
    }
    
    // Returning Fetched Records
    NSLog(@"[%s] returned %d", __FUNCTION__, fetchedRecords.count);
    return fetchedRecords.count;
}


// 1
- (NSManagedObjectContext *) managedObjectContext {
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    
    return _managedObjectContext;
}

//2
- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    
    return _managedObjectModel;
}

//3
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    NSURL *storeUrl = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory]
                                               stringByAppendingPathComponent: @"Recents.sqlite"]];
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]
                                   initWithManagedObjectModel:[self managedObjectModel]];
    if(![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                  configuration:nil URL:storeUrl options:nil error:&error]) {
        NSLog(@"[%s] ERROR - couldn't create core data storage!: %@", __FUNCTION__, error);
    }
    
    return _persistentStoreCoordinator;
}

- (NSString *)applicationDocumentsDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}


@end
